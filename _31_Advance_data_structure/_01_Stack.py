"""
Stack in Python:
===============
A stack is a linear data structure that stores items in a Last-In/First-Out (LIFO) or First-In/Last-Out (FILO) manner.
In stack, a new element is added at one end and an element is removed from that end only. The insert and delete
operations are often called push and pop.

Method of Stack :
=================
empty() – Returns whether the stack is empty – Time Complexity : O(1)
size() – Returns the size of the stack – Time Complexity : O(1)
top() – Returns a reference to the top most element of the stack – Time Complexity : O(1)
push(g) – Adds the element ‘g’ at the top of the stack – Time Complexity : O(1)
pop() – Deletes the top most element of the stack – Time Complexity : O(1)
Implementation
"""


class Stack:
    def __init__(self):
        self.stack = []

    def push(self, val):
        if val:
            self.stack.append(val)

    def pop(self):
        if len(self.stack) > 0:
            self.stack.pop()
        else:
            return None

    def __str__(self):
        return f"The output {self.stack}"


stack = Stack()

while True:
    print("1.To push item to stack")
    print("2.To pop item from stack")
    print("3.To exit")
    inp = int(input("enter any one option from above"))
    if inp == 1:
        item_inp = input("enter an item to push")
        stack.push(item_inp)
    elif inp == 2:
        stack.pop()
    else:
        print(stack)
        exit()
